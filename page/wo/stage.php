<?php
	header("Origin:xxx.com");
    header("Access-Control-Allow-Origin:*");
    include("conf.php");
    
    $con = new mysqli(host,user,pass,dbase);
	if($con -> connect_errno){
        printf("Connection error: %s\n", $con->connect_error);
    }

    $wo = '';
    $wo = $_POST['wo']; 
	$user = $_POST['user']; 
	$quser = 'SELECT id_group FROM tb_permit WHERE user_p="'.$user.'" AND application="Service and Maintenance"';
	$ruser = mysqli_query($con,$quser); 
	$nuser = mysqli_fetch_assoc($ruser);
	$idgroup = $nuser['id_group']; 
	
	if($idgroup=='GROUP181120033150'){
	    DEFINE('COMWOSTAT','SELECT WorkStatusID, WorkStatus FROM work_status');
	}else if($idgroup=='GROUP181120025602'){
		if(isset($wo)){
			DEFINE('COMWOSTAT','
			SELECT WorkStatusID, WorkStatus FROM work_status WHERE WorkStatusID IN (SELECT WorkStatusID FROM work_order WHERE WorkOrderNo="'.$wo.'")
			UNION
			SELECT WorkStatusID, WorkStatus FROM work_status WHERE WorkStatusID="WS000001" OR WorkStatusID="WS000022" OR WorkStatusID="WS000021"');
		}else{
			DEFINE('COMWOSTAT','SELECT WorkStatusID, WorkStatus FROM work_status WHERE WorkStatusID="WS000001" OR WorkStatusID="WS000022" OR WorkStatusID="WS000021"');
		}
	}else{
		if(isset($wo)){
			DEFINE('COMWOSTAT','
			SELECT WorkStatusID, WorkStatus FROM work_status WHERE WorkStatusID IN (SELECT WorkStatusID FROM work_order WHERE WorkOrderNo="'.$wo.'")
			UNION
			SELECT WorkStatusID, WorkStatus FROM work_status where id_group ="'.$idgroup.'" OR id_group =""');
		}else{
			DEFINE('COMWOSTAT','SELECT WorkStatusID, WorkStatus FROM work_status where id_group ="'.$idgroup.'" OR id_group =""');
		}
	}
	
    //---- Get status for work order----//
    $qu = 'SELECT WorkStatusID FROM work_order WHERE WorkOrderNo="'.$wo.'"';
    $res = mysqli_query($con,$qu); 
    $dat = mysqli_fetch_assoc($res);
    $wo_stage = $dat['WorkStatusID'];

    //---- Get all status stage in workstatus table---//
    //$query = 'SELECT WorkStatusID, WorkStatus FROM work_status';
	$query = COMWOSTAT;
    $result = mysqli_query($con,$query); $content_table = '';
    while($data = mysqli_fetch_assoc($result)){
        if($data['WorkStatusID']==$wo_stage)
            $content_table .= '<option class="full" value="'.$data['WorkStatusID'].'" selected>'.$data['WorkStatus'].'</option>'; 
        else
            $content_table .= '<option class="full" value="'.$data['WorkStatusID'].'">'.$data['WorkStatus'].'</option>'; 
    }

    $content = '
        <div class="row vertical-align-center" id="combo_up">
            <div class="col">
                <div class="list">
                    <div class="item">
                        <select id="comb_state">
                            '.$content_table.'
                        </select>
                    </div>
                </div>
                <a href="#" onclick="update_stage(\''.$wo.'\');" class="a_button orange full"><i class="icon ion-edit"></i>Update</a>
            </div>
        </div>
    ';

    echo $content;

?>